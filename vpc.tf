data "aws_vpc" "default" {
  default = true
}

resource "aws_subnet" "subnet" {
  vpc_id            = data.aws_vpc.default.id
  cidr_block        = "172.31.30.0/24"
  availability_zone = "us-east-1a"

  tags = {
    name        = "${var.name}"
    Environment = "${var.environment}"
  }
}
